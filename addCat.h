///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
///
/// @file addCat.h
/// @version 1.0
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include "catDatabase.h"
#pragma once


//headerfile for adding cats

extern int addCat( const char CatName[], const enum catGender Gender, const enum catBreed Breed, const bool IsFixed, const float Weight, const enum Color CollarColor1, const enum Color CollarColor2, const unsigned long long License );

