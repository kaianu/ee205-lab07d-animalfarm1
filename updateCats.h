///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
///
/// @file updateCats.h
/// @version 1.0
///
/// function: add cats to database
///
/// @author Kaianu Reyes-Huynh <kaianu@hawaii.edu>
/// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#pragma once

int updateCatName(int index, char newName[]);

int fixCat(int index);

int updateCatWeight(int index, float newWeight);

int updateCatCollar1(int index, enum Color newCollar1);

int updateCatCollar2(int index, enum Color newCollar2);

int updateLicense(int index, unsigned long long newLicense);


